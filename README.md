# Garden API

## Description

Garden is the main garden API from nandawtek apps

## Requirements

- Node > v18.18.0
- npm > 10.2.4

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```
