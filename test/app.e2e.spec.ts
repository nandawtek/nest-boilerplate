import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication, Logger } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ROUTES } from '../src/shared/contracts/routes';
import { AppService } from '../src/app/services/app.service';
import { EmptyLogger } from './support/EmptyLogger';

describe('App', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(Logger)
      .useValue(new EmptyLogger())
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('API is healthy', () => {
    return request(app.getHttpServer())
      .get('/' + ROUTES.health)
      .expect(HttpStatus.OK);
  });
  it('has custom response for internal server errors', async () => {
    jest.spyOn(AppService.prototype, 'isHealthy').mockImplementation(() => {
      throw new Error('Custom error');
    });
    const response = await request(app.getHttpServer()).get(
      '/' + ROUTES.health,
    );

    expect(response.status).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    expect(response.body.message).toBe('Internal server error');
    expect(response.body.extraInfo).toEqual({});
    expect(response.body.errorId).toBeDefined();
    expect(response.body.path).toBe('/' + ROUTES.health);
    expect(response.body.statusCode).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    expect(response.body.timestamp).toBeDefined();
  });
});
