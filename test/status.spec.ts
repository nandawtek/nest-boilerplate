import { Status } from '../src/app/domain/Status';
import { InvalidArgumentException } from '../src/shared/domain/errors/InvalidArgumentException';

describe('Status', () => {
  it('should throw InvalidArgumentException for invalid Status', () => {
    const status = null;

    expect(() => new Status(status)).toThrow(InvalidArgumentException);
  });
});
