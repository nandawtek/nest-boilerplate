import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from '../src/infrastructure/app/app.controller';
import { Logger } from '@nestjs/common';
import { EmptyLogger } from './support/EmptyLogger';
import { AppModule } from '../src/app.module';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(Logger)
      .useValue(new EmptyLogger())
      .compile();

    appController = app.get<AppController>(AppController);
  });

  describe('API', () => {
    it('Is healthy"', () => {
      expect(appController.isHealthy().status).toBe('ok');
    });
  });
});
