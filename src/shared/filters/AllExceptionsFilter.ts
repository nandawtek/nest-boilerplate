import {
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Logger,
  Catch,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { randomUUID } from 'crypto';
import { CustomError } from '../domain/errors/CustomError';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(
    private readonly httpAdapterHost: HttpAdapterHost,
    private readonly logger: Logger,
  ) {}

  catch(exception: any, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;
    const response = host.switchToHttp().getResponse();

    const ctx = host.switchToHttp();

    const httpStatus = exception.status || HttpStatus.INTERNAL_SERVER_ERROR;

    const errorId = randomUUID();
    const error = new CustomError(
      exception.message,
      httpStatus,
      errorId,
      httpAdapter.getRequestUrl(ctx.getRequest()),
      exception.response,
    ).toPrimitives();

    this.logger.error(exception.stack || '', error);

    error.message =
      httpStatus !== HttpStatus.INTERNAL_SERVER_ERROR
        ? exception.message
        : 'Internal server error';

    response.status(httpStatus).json(error);
  }
}
