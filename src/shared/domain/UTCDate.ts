import { ValueObject } from './ValueObject';
import { InvalidArgumentException } from './errors/InvalidArgumentException';

export class UtcDate extends ValueObject<Date> {
  readonly value: Date;
  constructor(value = new Date().toISOString()) {
    super(new Date(value));
    this.ensureIsValidUtcDate(new Date(this.value));
  }

  private ensureIsValidUtcDate(value: Date): void {
    if (isNaN(value.getTime()) || value.toISOString() !== value.toISOString()) {
      throw new InvalidArgumentException(
        `<${this.constructor.name}> does not allow the value <${value}> as a valid UTC date.`,
      );
    }
  }
}
