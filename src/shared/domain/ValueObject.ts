import { InvalidArgumentException } from './errors/InvalidArgumentException';

type Primitives = string | number | boolean | Date | unknown;

export abstract class ValueObject<T extends Primitives> {
  readonly value: T;

  constructor(value: T) {
    this.value = value;
    this.ensureValueIsDefined(value);
  }

  private ensureValueIsDefined(value: T): void {
    if (value === null || value === undefined) {
      throw new InvalidArgumentException(
        `Value on <${this.constructor.name}> must be defined`,
      );
    }
  }

  equals(other: ValueObject<T>): boolean {
    return (
      other.constructor.name === this.constructor.name &&
      other.value === this.value
    );
  }

  toString(): string {
    if (this.value instanceof Date) return this.value.toISOString();
    if (typeof this.value === 'object') return JSON.stringify(this.value);
    return this.value.toString();
  }
}
