import { ValueObject } from '../ValueObject';

export class ExtraInfo extends ValueObject<unknown> {
  constructor(readonly value: unknown = {}) {
    super(value);
  }
}
