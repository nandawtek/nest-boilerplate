import { HttpStatus } from '@nestjs/common';

export class InvalidArgumentException extends Error {
  private readonly statusCode: number;
  constructor(message: string) {
    super(`[InvalidArgumentException]: ${message}`);
    this.statusCode = HttpStatus.NOT_ACCEPTABLE;
    this.name = 'InvalidArgumentException';
  }
}
