import { ValueObject } from '../ValueObject';

export class Message extends ValueObject<string> {
  constructor(readonly value: string) {
    super(value);
  }
}
