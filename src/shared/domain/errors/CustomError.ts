import { Message } from './Message';
import { UtcDate } from '../UTCDate';
import { StatusCode } from './StatusCode';
import { Uuid } from '../Uuid';
import { Path } from './Path';
import { ExtraInfo } from './ExtraInfo';
export type CustomErrorPrimitives = {
  statusCode: number;
  timestamp: string;
  path: string;
  errorId: string;
  message: string;
  extraInfo: unknown;
};
export class CustomError {
  private readonly timestamp: UtcDate;
  private readonly message: Message;
  private readonly statusCode: StatusCode;
  private readonly errorId: Uuid;
  private readonly path: Path;
  private readonly extraInfo: ExtraInfo;
  constructor(
    message: string,
    statusCode: number,
    errorId: string,
    path: string,
    extraInfo: unknown,
    timestamp?: string,
  ) {
    this.message = new Message(message);
    this.statusCode = new StatusCode(statusCode);
    this.errorId = new Uuid(errorId);
    this.path = new Path(path);
    this.timestamp = new UtcDate(timestamp);
    this.extraInfo = new ExtraInfo(extraInfo);
  }
  toPrimitives(): CustomErrorPrimitives {
    return {
      statusCode: this.statusCode.value,
      timestamp: this.timestamp.toString(),
      path: this.path.value,
      errorId: this.errorId.value,
      message: this.message.value,
      extraInfo: this.extraInfo.value,
    };
  }
  fromPrimitives({
    message,
    statusCode,
    errorId,
    path,
    extraInfo,
    timestamp,
  }: CustomErrorPrimitives) {
    return new CustomError(
      message,
      statusCode,
      errorId,
      path,
      extraInfo,
      timestamp,
    );
  }
}
