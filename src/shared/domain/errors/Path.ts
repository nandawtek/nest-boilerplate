import { ValueObject } from '../ValueObject';

export class Path extends ValueObject<string> {
  constructor(readonly value: string) {
    super(value);
  }
}
