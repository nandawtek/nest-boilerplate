import { ValueObject } from '../ValueObject';

export class StatusCode extends ValueObject<number> {
  constructor(readonly value: number) {
    super(value);
  }
}
