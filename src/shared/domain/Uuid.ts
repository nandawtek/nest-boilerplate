import { v4 as uuid } from 'uuid';
import * as validate from 'uuid-validate';
import { InvalidArgumentException } from './errors/InvalidArgumentException';
import { ValueObject } from './ValueObject';

export class Uuid extends ValueObject<string> {
  constructor(readonly value = uuid()) {
    super(value);
    this.ensureIsValidUuid(value);
  }

  static random(): Uuid {
    return new Uuid();
  }

  private ensureIsValidUuid(id: string): void {
    if (!validate(id)) {
      throw new InvalidArgumentException(
        `<${this.constructor.name}> does not allow the value <${id}>`,
      );
    }
  }
}
