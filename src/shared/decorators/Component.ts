import { Injectable, ScopeOptions } from '@nestjs/common';
export function Component(options?: ScopeOptions): ClassDecorator {
  return Injectable(options);
}
