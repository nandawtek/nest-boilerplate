import { ValueObject } from '../../shared/domain/ValueObject';

export class Status extends ValueObject<string> {
  constructor(readonly value: string) {
    super(value);
  }
}
