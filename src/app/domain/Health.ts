import { Status } from './Status';
export type HealthPrimitives = {
  status: Status;
};

export class Health {
  private readonly status: Status;
  constructor() {
    this.status = new Status('ok');
  }

  toPrimitives() {
    return {
      status: this.status.value,
    };
  }
  fromPrimitives(healthPrimitives: HealthPrimitives) {
    return {
      status: healthPrimitives.status,
    };
  }
}
