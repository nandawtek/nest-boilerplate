import { HealthResult } from '../domain/contracts/health.result';
import { Health } from '../domain/Health';
import { Component } from '../../shared/decorators/Component';

@Component()
export class AppService {
  isHealthy(): HealthResult {
    return new Health().toPrimitives();
  }
}
