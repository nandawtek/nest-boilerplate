import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from '../../app/services/app.service';
import { HealthResult } from '../../app/domain/contracts/health.result';
import { ROUTES } from '../../shared/contracts/routes';
import { ConfigService } from '@nestjs/config';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService,
  ) {}

  @Get(ROUTES.health)
  isHealthy(): HealthResult {
    this.logger.log(
      `Checking health on ${this.configService.get('environment')}`,
    );
    return this.appService.isHealthy();
  }
}
