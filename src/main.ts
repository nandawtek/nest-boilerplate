import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import helmet from 'helmet';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const port = configService.get('port');
  app.use(helmet());
  addSwagger(app);
  await app.listen(port).then(() => {
    logger.log(`Application is running on port ${port}`);
  });
}
function addSwagger(app: INestApplication<any>) {
  const config = new DocumentBuilder()
    .setTitle('Garden API')
    .setDescription('Main Garden info')
    .setVersion('1.0')
    .addTag('Garden')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
}
bootstrap();
