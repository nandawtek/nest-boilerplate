import { Logger, Module } from '@nestjs/common';
import { AppController } from './infrastructure/app/app.controller';
import { AppService } from './app/services/app.service';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from './shared/filters/AllExceptionsFilter';
import configuration from './config/configuration';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
    }),
  ],
  controllers: [AppController],
  providers: [
    Logger,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    AppService,
  ],
})
export class AppModule {}
